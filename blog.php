<?php include "inc_header_2.php" ?>
<link rel="stylesheet" href="css/user.style.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<style>
	.logo{
		width:250px;
	}
</style>
<div class="bg_content pagestyle  ">
	<div class="container search-bar horizontal collapse in">
		<?php include "inc_search.php" ?>
	</div>
</div>
			<div id="page-content">
                <section class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <header>
                                <h1 class="page-title">Blog</h1>
                            </header>

                            <article class="blog-post">
                                <header><a href="blog-detail.html"><h2>Vivamus porta orci eu turpis vulputate ornare fusce hendrerit arcu risu</h2></a></header>
                                <a href="blog-detail.html"><img src="images/blog-1.jpg" alt=""></a>
                                <figure class="meta">
                                    <a href="#" class="link-icon"><i class="fa fa-user"></i>Admin</a>
                                    <a href="#" class="link-icon"><i class="fa fa-calendar"></i>06/04/2014</a>
                                    <div class="tags">
                                        <a href="#" class="tag article">Architecture</a>
                                        <a href="#" class="tag article">Design</a>
                                        <a href="#" class="tag article">Trend</a>
                                    </div>
                                </figure>
                                <p>Fusce quis nulla volutpat, rhoncus ligula ut, pulvinar nisi. In dapibus urna sit amet accumsan
                                    tristique. Donec odio ligula, luctus venenatis varius id, tincidunt ac ipsum. Cras commodo,
                                    velit nec aliquam dictum, tortor velit dictum ipsum, sed ornare nunc leo nec ipsum. Vestibulum
                                    sagittis sapien vitae tristique mollis. Aliquam hendrerit nulla semper, viverra mi et,
                                    hendrerit mauris. Maecenas hendrerit congue ultrices. In laoreet erat blandit eros aliquet,
                                    in malesuada sem rutrum. In placerat porta egestas.
                                </p>
                                <a href="blog-detail.html" class="icon">Read More <i class="fa fa-angle-right"></i></a>
                            </article><!-- /.blog-post -->
                            <article class="blog-post">
                                <header><a href="blog-detail.html"><h2>Nulla sapien leo, placerat sed lacinia nec, rutrum quis</h2></a></header>
                                <a href="blog-detail.html"><img src="images/blog-2.jpg" alt=""></a>
                                <figure class="meta">
                                    <a href="#" class="link-icon"><i class="fa fa-user"></i>Admin</a>
                                    <a href="#" class="link-icon"><i class="fa fa-calendar"></i>06/04/2014</a>
                                    <div class="tags">
                                        <a href="#" class="tag article">Interior</a>
                                        <a href="#" class="tag article">New Living</a>
                                    </div>
                                </figure>
                                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                    Donec rutrum imperdiet ligula in bibendum. Aenean vulputate rutrum lobortis. Nullam non
                                    mi ac dui egestas venenatis. Etiam venenatis fermentum accumsan. Lorem ipsum dolor
                                    sit amet, consectetur adipiscing elit. Donec at lacus sapien.
                                </p>
                                <a href="blog-detail.html" class="link-arrow">Read More</a>
                            </article><!-- /.blog-post -->
                            <article class="blog-post">
                                <header><a href="blog-detail.html"><h2>SoundCloud Audio Post</h2></a></header>
                                <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/71654970&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=true&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                <figure class="meta">
                                    <a href="#" class="link-icon"><i class="fa fa-user"></i>Admin</a>
                                    <a href="#" class="link-icon"><i class="fa fa-calendar"></i>06/04/2014</a>
                                    <div class="tags">
                                        <a href="#" class="tag article">Audio</a>
                                        <a href="#" class="tag article">SoundCloud</a>
                                    </div>
                                </figure>
                                <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                    Donec rutrum imperdiet ligula in bibendum. Aenean vulputate rutrum lobortis. Nullam non
                                    mi ac dui egestas venenatis. Etiam venenatis fermentum accumsan. Lorem ipsum dolor
                                    sit amet, consectetur adipiscing elit. Donec at lacus sapien.
                                </p>
                                <a href="blog-detail.html" class="link-arrow">Read More</a>
                            </article><!-- /.blog-post -->
                            <article class="blog-post">
                                <header><a href="blog-detail.html"><h2>Cras commodo, velit nec aliquam dictum, tortor velit
                                    dictum ipsum, sed ornare nunc leo nec ipsum. Vestibulum sagittis sapien vitae tristique mollis.</h2></a></header>
                                <figure class="meta">
                                    <a href="#" class="link-icon"><i class="fa fa-user"></i>Admin</a>
                                    <a href="#" class="link-icon"><i class="fa fa-calendar"></i>06/04/2014</a>
                                    <div class="tags">
                                        <a href="#" class="tag article">Interior</a>
                                        <a href="#" class="tag article">New Living</a>
                                    </div>
                                </figure>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet commodo mauris,
                                    sit amet commodo turpis. Duis consequat placerat lacus, in sagittis metus pretium vel.
                                    In luctus justo venenatis, accumsan justo sit amet, volutpat dolor. Pellentesque quis nulla
                                    nec nisi tempor scelerisque. Nam nec scelerisque sapien. Donec eleifend purus id neque pretium,
                                    at sollicitudin erat vestibulum. Donec ac tempus tellus, ac dignissim sapien. Fusce et
                                    elementum arcu. Maecenas sit amet tincidunt lorem.
                                </p>
                                <p>Vivamus porta orci eu turpis vulputate ornare. Fusce hendrerit arcu risus, sed commodo
                                    lacus viverra in. Donec eget ligula in risus rutrum pretium id a elit. Nullam ut tristique
                                    arcu. Nam quis nunc ac eros accumsan tincidunt vel sit amet lorem. Sed euismod, turpis
                                    et facilisis vestibulum, leo lectus consectetur velit, sed lobortis ante dolor nec leo.
                                    Praesent congue tellus eu dui consectetur commodo. Sed quam ante, elementum sodales felis
                                    quis, rutrum tincidunt dolor. Etiam nec metus iaculis arcu cursus pulvinar. Nunc interdum
                                    eros a neque elementum lobortis. Nulla mattis quis risus vel molestie. Mauris id urna ac
                                    metus blandit lobortis in et odio.
                                </p>
                                <a href="blog-detail.html" class="link-arrow">Read More</a>
                            </article><!-- /.blog-post -->

                            <!--Pagination-->
                            <nav>
                                <ul class="pagination pull-right">
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#" class="previous"><i class="fa fa-angle-left"></i></a></li>
                                    <li><a href="#" class="next"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </nav>
                            <!--end Pagination-->
                        </div>
                        <div class="col-md-3">
                            <aside id="sidebar">
                                <section>
                                    <header><h2>New Places</h2></header>
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Cash Cow Restaurante</h3>
                                        <figure>63 Birch Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="images/items/1.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                                <div class="rating" data-rating="4"></div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Blue Chilli</h3>
                                        <figure>2476 Whispering Pines Circle</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="images/items/2.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                                <div class="rating" data-rating="3"></div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Eddie�s Fast Food</h3>
                                        <figure>4365 Bruce Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="images/items/3.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                                <div class="rating" data-rating="5"></div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                </section>
                                <section>
                                    <a href="#"><img src="images/ad-banner-sidebar.png" alt=""></a>
                                </section>
                                <section>
                                    <header><h2>Categories</h2></header>
                                    <ul class="bullets">
                                        <li><a href="#" >Restaurant</a></li>
                                        <li><a href="#" >Steak House & Grill</a></li>
                                        <li><a href="#" >Fast Food</a></li>
                                        <li><a href="#" >Breakfast</a></li>
                                        <li><a href="#" >Winery</a></li>
                                        <li><a href="#" >Bar & Lounge</a></li>
                                        <li><a href="#" >Pub</a></li>
                                    </ul>
                                </section>
                                <section>
                                    <header><h2>Events</h2></header>
                                    <div class="form-group">
                                        <select class="framed" name="events">
                                            <option value="">Select Your City</option>
                                            <option value="1">London</option>
                                            <option value="2">New York</option>
                                            <option value="3">Barcelona</option>
                                            <option value="4">Moscow</option>
                                            <option value="5">Tokyo</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </section>
                            </aside>
                            <!-- /#sidebar-->
                        </div>
                        <!-- /.col-md-3-->
                    </div>
                </section>
            </div>
</div>

<?php include "inc_footer.php" ?>
<script>
	var $ = jQuery.noConflict();
    if( $('body').hasClass('navigation-fixed') ){
        $('.off-canvas-navigation').css( 'top', - $('.header').height() );
        $('#page-canvas').css( 'margin-top',$('.header').height() );
    }
	$(document).ready(function($) {
		$('.off-canvas-navigation header').css( 'line-height', $('.header').height() + 'px' );
		"use strict";
		$(document).bind('keypress', 'M', function(){ 
			$('.header .toggle-navigation').trigger('click');
			return false;
		});
	});
</script>
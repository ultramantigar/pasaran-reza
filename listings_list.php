<?php include "inc_header.php" ?>
 
<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		<?php include "inc_search.php" ?>
	</div> 
	<div class="container"> 
			<figure class="filter "> 
						<div class="pull-left">
							<ol class="breadcrumb">
								<li><a href="index-directory.html"><i class="fa fa-home"></i></a></li>
								<li><a href="#">Pencarian</a></li>
								<li class="active">List</li>
							</ol> 
						</div>
						<div class="buttons pull-right">
							<a href="listing-grid.html" class="btn icon "><i class="fa fa-th"></i>Grid</a>
							<a href="listing-list.html" class="btn icon active"><i class="fa fa-th-list"></i>List</a>
							<a href="listing-maps.html" class="btn icon "><i class="fa fa-map-marker"></i>Map</a>
						</div>
						<div class="pull-right">
							<aside class="sorting">
								<span>Sorting</span>
								<div class="form-group">
									<select class="selectpicker" data-live-search="true">
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select> 
								</div> 
							</aside>
						</div> 
			</figure> 
			<div class="clear"></div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 
				<div class="row">
					<?php for($a=1;$a<=10;$a++){ ?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="sl_item list"> 
								<div class="sl_item_img" style="background:url('images/items/img_steakhouse.jpg') no-repeat center center;background-size: cover;">
									<a class="icon_like"><i class="fa fa-thumbs-up"></i></a>
									<a class="icon_view"><i class="fa fa-eye"></i>
										<span class="count_view">Dilihat 1000 kali</span>
									</a>
									<!-- overlay -->
									<div class="content_overlay_bg"> </div>
									<div class="content_overlay"> </div>
									<div class="content_overlay_inner">
										<h5>Deskripsi</h5>
										<p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
									</div>
									<!-- end overlay -->
									<div class="time_label">2 Menit Lalu</div>
								</div>
								<div class="sl_item_detail col-lg-9 col-md-9 col-sm-8 col-xs-12">
									<a href="item-detail.html"><h3>Steak House Restaurant</h3></a>
									<figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
									<div class="price">Rp. 20</div>
									<div class="info">
										<div class="type">
											<i><img src="images/material/restaurant.png" alt=""></i>
											<span>Restaurant</span>
										</div>
									</div>
								</div> 
								<div class="clear"></div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<ul class="pagination pull-right">
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#" class="previous"><i class="fa fa-angle-left"></i></a></li>
							<li><a href="#" class="next"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div> 
				</div> 
			</div>
		</div>
	</div>
</div>

<?php include "inc_footer.php" ?>
<?php include "inc_header.php" ?>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/user.style.css" type="text/css">
<style>
	.logo{
		width:250px;
	}
	
</style>
<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		<?php include "inc_search.php" ?>
	</div>

        <div id="page-canvas"> 
            <div id="page-content">
                <!--/.container-->
                <section id="image">
                    <div class="container">
                        <div class="col-md-9 col-sm-offset-2">
                            <div class="text-banner">
                                <figure>
                                    <img src="images/marker.png" alt="">
                                </figure>
                                <div class="description">
                                    <h2>Situs Pemasaran No.1 di Indonesia</h2>
                                    <p>
                                        Suspendisse potenti. Integer quis eleifend neque. Curabitur lobortis dictum mollis.
                                        In rhoncus sapien eget tellus sodales.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.container-->
                    <div class="background">
                       <img src="images/pasaran-bg.png" alt="Pasaran.com">>
                    </div>
                    <!--/.bakcground-->
                </section>
                <section class="block background-color-grey-dark" id="features">
                    <div class="container">
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                               <div class="feature-box">
                                   <i class="fa fa-search"></i>
                                   <div class="description">
                                       <h3>Pencarian Lengkap</h3>
                                       <p>
                                           Praesent tempor a erat in iaculis. Phasellus vitae libero libero. Vestibulum ante
                                           ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                                       </p>
                                   </div>
                               </div>
                               <!--/.feature-box-->
                           </div>
                           <!--/.col-md-4-->
                           <div class="col-md-4 col-sm-4">
                               <div class="feature-box">
                                   <i class="fa fa-map"></i>
                                   <div class="description">
                                       <h3>Pencarian Map</h3>
                                       <p>
                                           Pellentesque nisl quam, aliquet sed velit eu, varius condimentum nunc.
                                           Nunc vulputate turpis ut erat egestas, vitae rutrum sapien varius.
                                       </p>
                                   </div>
                               </div>
                               <!--/.feature-box-->
                           </div>
                           <!--/.col-md-4-->
                           <div class="col-md-4 col-sm-4">
                               <div class="feature-box">
                                   <i class="fa fa-money"></i>
                                   <div class="description">
                                       <h3>Pemasangan Iklan Gratis</h3>
                                       <p>
                                           Maecenas quis ipsum lectus. Fusce molestie, metus ut consequat pulvinar,
                                           ipsum quam condimentum leo, sit amet auctor lacus nulla at felis.
                                       </p>
                                   </div>
                               </div>
                               <!--/.feature-box-->
                           </div>
                           <!--/.col-md-4-->
                       </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </section>
                <!--/.block-->
                <section class="block" id="the-team">
                    <div class="container">
                        <header class="no-border" style="text-align: center; padding-top: 30px; "><h1>Pasaran Team</h1></header>
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <div class="member">
                                    <img src="images/member-1.jpg" alt="">
                                    <h4>Jane Daubert</h4>
                                    <figure>Company CEO</figure>
                                    <hr>
                                    <div class="social">
                                        <a href="#" ><i class="fa fa-twitter"></i></a>
                                        <a href="#" ><i class="fa fa-facebook"></i></a>
                                        <a href="#" ><i class="fa fa-pinterest"></i></a>
                                    </div>
                                </div>
                                <!--/.member-->
                            </div>
                            <!--/.col-md-3-->
                            <div class="col-md-3 col-sm-3">
                                <div class="member">
                                    <img src="images/member-2.jpg" alt="">
                                    <h4>Kristy Jose</h4>
                                    <figure>Marketing Specialist</figure>
                                    <hr>
                                    <div class="social">
                                        <a href="#" ><i class="fa fa-twitter"></i></a>
                                        <a href="#" ><i class="fa fa-facebook"></i></a>
                                        <a href="#" ><i class="fa fa-pinterest"></i></a>
                                    </div>
                                </div>
                                <!--/.member-->
                            </div>
                            <!--/.col-md-3-->
                            <div class="col-md-3 col-sm-3">
                                <div class="member">
                                    <img src="images/member-3.jpg" alt="">
                                    <h4>John Doe</h4>
                                    <figure>PR Manager</figure>
                                    <hr>
                                    <div class="social">
                                        <a href="#" ><i class="fa fa-twitter"></i></a>
                                        <a href="#" ><i class="fa fa-facebook"></i></a>
                                        <a href="#" ><i class="fa fa-pinterest"></i></a>
                                    </div>
                                </div>
                                <!--/.member-->
                            </div>
                            <!--/.col-md-3-->
                            <div class="col-md-3 col-sm-3">
                                <div class="member">
                                    <img src="images/member-4.jpg" alt="">
                                    <h4>Misty Bates</h4>
                                    <figure>Support</figure>
                                    <hr>
                                    <div class="social">
                                        <a href="#" ><i class="fa fa-twitter"></i></a>
                                        <a href="#" ><i class="fa fa-facebook"></i></a>
                                        <a href="#" ><i class="fa fa-pinterest"></i></a>
                                    </div>
                                </div>
                                <!--/.member-->
                            </div>
                            <!--/.col-md-3-->
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </section>
                <!--/.block-->
                <section class="block background-color-white" id="testimonials">
                    <div class="container">
                        <div class="owl-carousel testimonials">
                            <blockquote>
                                <figure><img src="images/client.jpg" alt=""></figure>
                                <div class="description">
                                    <p>
                                        Fusce risus metus, placerat in consectetur eu, porttitor a est. Sed sed dolor lorem. Cras adipiscing
                                    </p>
                                    <footer>Natalie Jenkins</footer>
                                </div>
                            </blockquote>
                            <blockquote>
                                <figure><img src="images/client.jpg" alt=""></figure>
                                <div class="description">
                                    <p>
                                        Fusce risus metus, placerat in consectetur eu, porttitor a est. Sed sed dolor lorem. Cras adipiscing
                                    </p>
                                    <footer>Natalie Jenkins</footer>
                                </div>
                            </blockquote>
                        </div>
                        <!--/.testimonials-->
                    </div>
                    <!--/.container-->
                </section>
                <!--/.testimonials-->
                <section id="partners" class="block">
                    <div class="container">
                        <header class="no-border"><h3>Partners</h3></header>
                        <div class="logos">
                            <div class="logo"><a href=""><img src="images/logo-partner-01.png" alt=""></a></div>
                            <div class="logo"><a href=""><img src="images/logo-partner-02.png" alt=""></a></div>
                            <div class="logo"><a href=""><img src="images/logo-partner-03.png" alt=""></a></div>
                            <div class="logo"><a href=""><img src="images/logo-partner-04.png" alt=""></a></div>
                            <div class="logo"><a href=""><img src="images/logo-partner-05.png" alt=""></a></div>
                        </div>
                        <!--/.logos-->
                    </div>
                    <!--/.container-->
                </section>
                <!--./partners-->
            </div>
            <!-- end Page Content-->
        </div>
	</div>

<?php include "inc_footer.php" ?>

<script> 
</script>
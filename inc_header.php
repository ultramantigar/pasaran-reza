<!DOCTYPE html>
<html lang="en"> 
<head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   <title>Pasaran.com - Situs Pemasaran No.1 Di Indonesia</title> 
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="fonts/font-awesome/font-awesome.css">
	<link rel="stylesheet" href="css/dropzone.css" type="text/css">
	
	<link href="fonts/font-awesome.css" rel="stylesheet" type="text/css"> 
	<link rel="stylesheet" type="text/css" href="css/set1.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
	<link rel="stylesheet" href="css/owl.carousel.css"> 
    <link href="css/scrolling-nav.css" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap-select.min.css"> 
    <link href="css/custom.css" rel="stylesheet">
	<link href="css/bootstrap-social.css" rel="stylesheet"> 

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
 
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<div class="header_top shadow">
	<div class="container"> 
			<div class="ht_left">
				<div class="ht_left_brand">
					<a href="#">
						<img src="images/material/logo/pasaran.png" class="logo" alt="Pasaran Logo">
						<span class="tagline"> Situs Pemasaran No.1 di Indonesia </span>
					</a>
				</div>
			</div>
			<div class="ht_right">
				<div class="ht_user_area">
					<ul>
						<li><a href=""><i class="fa fa-thumbs-up fa-1x pull-left"></i><strong>2</strong></a></li>
						<li><a href="profile.php?t=1"><i class="fa fa-user fa-1x pull-left"></i><strong>Akun Saya</strong></a></li>
					</ul>
				</div>
			</div> 
	</div>
</div>

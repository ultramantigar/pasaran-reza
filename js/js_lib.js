/*carousel*/
function slidepopuler(){ 
	 
	 
	$(".owl-populer").owlCarousel({
		autoPlay: 4100, //Set AutoPlay to 3 seconds
		items : 1,
		lazyLoad : true,
		navigation : true,
		autoWidth:true,
		loop:true,
	});
	
}

function selectStyle(){
	$('.selectpicker').selectpicker(); 
}
 
/*parallax*/
$.fn.isOnScreen = function () {
    if (this.length) {
        var viewport = {};
        viewport.top = $(window).scrollTop();
        viewport.bottom = viewport.top + $(window).height();
        var bounds = {};
        bounds.top = this.offset().top;
        bounds.bottom = bounds.top + this.outerHeight();
        return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
    } else
        return false;
};

$.fn.parralaxCon = function (e) {
    if ($(window).width() > 768) {
        var elem = this,
                speed = e.speed;

        elem.each(function () {
            if ($(this).length) {
                var distance = e.distance_move;
                var top = $(window).scrollTop();
                var top2 = $(this).offset().top;
                if ($(this).isOnScreen()) {
                    top2 = top2 - $(window).height();
                    top = top - top2;
                    var tambah = top * distance;

                    TweenMax.to($(this), speed, {
                        css: {
                            backgroundPosition: 'center' + ' -' + tambah + 'px'
                            
                        },
                        ease: Cubic.easeOut
                    });
                }
            }
        })
    }
}

/*Scrolltotop*/
function scrolltotop(){
    var btnscroll = $('#scrolltotop');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            TweenMax.to(btnscroll, 1,{
                css:{
                    display:'block',
                    opacity:'1'
                }, 
                ease:Quart.easeOut,
            });  
        } else {
             TweenMax.to(btnscroll,1,{
                css:{
                    position:'none',
                    opacity:'0'         
                }, 
                ease:Quart.easeOut,
            });    
                    
        }
    });
}
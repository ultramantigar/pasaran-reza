<?php include "inc_header.php" ?>
<?php include "inc_menu.php" ?>
<div class="bg_content">
	<section class="section_search" style="background:#fff url('images/material/pasaran-bg.png') no-repeat top center; background-size:cover">
		<div class="container">
			<h2>Cari kebutuhan apapun di manapun</h2>
			<div class="clear"></div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 marginauto" >
				<div class="row">
					<form class="form-inline" role="form">
						<div class="rw">
							<div class="form-group col-lg-6 col-md-6 col-sm-5 col-xs-12 pd5"> 
								<input type="text" class="form-control" name="katakunci" placeholder="Cari Kata Kunci : Kursi, Rumah, Mobil dll"/>
							</div>
							<div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12 pd5"> 
								<div class="input-group">
									<input type="text" class="form-control" name="lokasi" placeholder="Silahkan Pilih Lokasi ..."/> 
								</div>  
							</div>  
							<div class="form-group col-lg-1 col-md-1 col-sm-2 col-xs-12 pd5"> 
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div> 
						<div class="clear"></div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<div class="clear"></div>
	<section class="section_list_items section_style">
		<div class="container">
			<h3>TERBARU</h3>
			<div class="clear"></div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<?php for($a=1;$a<5;$a++){ ?>
								<div class="col-lg-3 col-md-3 col-sm-2 col-xs-12">
									<div class="sl_item"> 
										<div class="sl_item_img" style="background:url('images/items/img_steakhouse.jpg') no-repeat center center;background-size: cover;">
											<a class="icon_like"><i class="fa fa-thumbs-up"></i></a>
											<a class="icon_view"><i class="fa fa-eye"></i>
												<span class="count_view">Dilihat 1000 kali</span>
											</a>
											<!-- overlay -->
												<div class="content_overlay_bg"> </div>
												<div class="content_overlay"> </div>
												<div class="content_overlay_inner">
													<h5>Deskripsi</h5>
													<p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
												</div>
											<!-- end overlay -->
											<div class="time_label">2 Menit Lalu</div>
										</div>
										<div class="sl_item_detail">
											<a href="item-detail.php"><h3>Steak House Restaurant</h3></a>
											<figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
											<div class="price">Rp. 20</div>
											<div class="info">
												<div class="type">
													<i><img src="images/material/restaurant.png" alt=""></i>
													<span>Restaurant</span>
												</div>
											</div>
										</div> 
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
		</div>
	</section>
	<div class="clear"></div>
	<section class="section_populer section_style">
		<div class="container">
			<h3>POPULER</h3>
			 
			<div class="clear"></div>
				<div class="row"> 
					<div class="section_populer_slide">
						<!--navigation-->
						<div class="nav_left_populer"> </div>
						<div class="nav_right_populer"> </div>
						<!---->
						
						<div class="owl-populer">
						<?php for($a=1;$a<=5;$a++){ ?>
							<div class="item">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12"> 
											<img src="images/items/img_steakhouse.jpg" class="img-responsive" style="width:100%"/> 
										</div>
										<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
											<div class="wrap_r">
												<div class="row_btn rb_head"> 
													<a href="#"><h4>Magma Bar and Grill</h4></a>
													<div class="fav pull-right">
														<div class="type">
															<a href="#"><i class="fa fa-thumbs-up fa-lg"></i></a>
														</div>
													</div>
												</div>
												
												
												<figure>
													<i class="fa fa-map-marker"></i>
													<span>970 Chapel Street, Rosenberg, TX 77471</span>
												</figure>
												<div class="clear"></div>
												<div class="row_btn"> 
													<div class="price">Rp. 2.000.000</div> 
													<div class="type pull-right">
														<i><img src="images/material/restaurant.png"></i>
														<span>Restaurant</span>
													</div>
												</div>
												<div class="clear"></div>
												<p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa, viverra interdum eros ut,
													imperdiet pellentesque mauris. Proin sit amet scelerisque risus. Donec semper semper erat ut mollis.
													Curabitur suscipit, justo eu dignissim lacinia, ante sapien pharetra dui...
												</p>
												<a href="#" class="read-more">Read More</a>
											</div>
										</div>
									</div> 
								</div>
							</div> 
						<?php } ?>
						</div>
					</div>  	 
				</div>
		</div>
	</section>
	<div class="clear"></div>
	<section class="section_whyus section_style">
		<div class="container">
			<h3>WHY US?</h3>
			<div class="clear"></div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<div class="feature-box">
									<i class="fa fa-search red"></i>
									<div class="description">
										<h3>Pencarian Lengkap</h3>
										<p>
                                           Praesent tempor a erat in iaculis. Phasellus vitae libero libero. Vestibulum ante
                                           ipsum primis in faucibus orci luctus et ultrices posuere cubilia
										</p>
									</div>
								</div> 
							</div> 
							<div class="col-md-4 col-sm-4">
								<div class="feature-box">
									<i class="fa fa-map green"></i>
									<div class="description">
										<h3>Pencarian Map</h3>
										<p>
                                           Pellentesque nisl quam, aliquet sed velit eu, varius condimentum nunc.
                                           Nunc vulputate turpis ut erat egestas, vitae rutrum sapien varius.
										</p>
									</div>
								</div> 
							</div> 
							<div class="col-md-4 col-sm-4">
								<div class="feature-box">
									<i class="fa fa-money blue"></i>
									<div class="description">
										<h3>Pemasangan Iklan Gratis</h3>
										<p>
                                           Maecenas quis ipsum lectus. Fusce molestie, metus ut consequat pulvinar,
                                           ipsum quam condimentum leo, sit amet auctor lacus nulla at felis.
										</p>
									</div>
								</div> 
							</div> 
                       </div>
					</div>
				</div> 
		</div>
	</section>
	<div class="clear"></div>
	<hr>
	<div class="clear"></div> 
	<section class="section_populerhariini section_style">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h3>POPULER HARI INI</h3>
									<div class="clear"></div> 
									<div class="row">
										<?php for($a=1;$a<=6;$a++){ ?>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
												<div class="sl_item"> 
													<div class="sl_item_img" style="background:url('images/items/img_steakhouse.jpg') no-repeat center center;background-size: cover;">
														<a class="icon_like"><i class="fa fa-thumbs-up"></i></a>
														<a class="icon_view"><i class="fa fa-eye"></i>
															<span class="count_view">Dilihat 1000 kali</span>
														</a>
														<!-- overlay -->
															<div class="content_overlay_bg"> </div>
															<div class="content_overlay"> </div>
															<div class="content_overlay_inner">
																<h5>Deskripsi</h5>
																<p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
															</div>
														<!-- end overlay -->
														<div class="time_label">2 Menit Lalu</div>
													</div>
													<div class="sl_item_detail">
														<a href="item-detail.php"><h3>Steak House Restaurant</h3></a>
														<figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
														<div class="price">Rp. 20</div>
														<div class="info">
															<div class="type">
																<i><img src="images/material/restaurant.png" alt=""></i>
																<span>Restaurant</span>
															</div>
														</div>
													</div> 
												</div>
											</div>
										<?php } ?>
									</div> 
								</div>
							</div>
							
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 iklanpremium">
									<h3>IKLAN PREMIUM</h3>
									<div class="clear"></div>
									<?php for($a=1;$a<=5;$a++){ ?>
										<a href="#" class="item-horizontal small">
                                        	<div class="icon icon-on pull-right">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <b><h3>Honda Civic thn 2009</h3></b>
                                            <figure><i class="fa fa-map-marker"></i> Jakarta Barat</figure>
                                            <div class="wrapper">
                                                <div class="image"><img src="images/items/1.jpg" alt="" class="img-responsive"></div>
                                                <div class="info">
                                                	<div class="price"> 
                                                        <span>Rp. 2.000.000</span>
                                                    </div>
                                                    <div class="type">
                                                        <i><img src="images/material/restaurant.png" alt=""></i>
                                                        <span>Restaurant</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</section>
	
<div class="clear"></div>
</div>
<?php include "inc_footer.php" ?>
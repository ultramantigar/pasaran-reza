<?php include "inc_header_2.php" ?>
<link rel="stylesheet" href="css/user.style.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<style>
	.logo{
		width:250px;
	}
</style>
<div class="bg_content pagestyle  ">
	<div class="container search-bar horizontal collapse in">
		<?php include "inc_search.php" ?>
	</div>
</div>
 
			<div id="page-content">
                <section class="container">
                    <div class="row">
                        <!--Content-->
                        <div class="col-md-9">
                            <header>
                                <h1 class="page-title">FAQ</h1>
                            </header>
                            <section class="faq-form">
                                <figure class="clearfix">
                                    <i class="fa fa-question"></i>
                                    <div class="wrapper">
                                        <div class="pull-left">
                                            <strong>Didn't find an answer?</strong>
                                            <h3>Ask Your Question</h3>
                                        </div>
                                        <a href="#form-faq" class="btn btn-default pull-right" data-toggle="collapse" aria-expanded="false" aria-controls="form-faq">Ask Question</a>
                                    </div>
                                </figure>
                                <div class="collapse" id="form-faq">
                                    <div class="">
                                        <form role="form" action="?" method="post">
                                            <div class="form-group">
                                                <label for="faq-form-email">Email</label>
                                                <input type="email" class="form-control" id="faq-form-email" required="">
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="faq-form-question">Question</label>
                                                <textarea class="form-control" id="faq-form-question" name="faq-form-question"  rows="3" required=""></textarea>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-default">Submit Question</button>
                                            </div>
                                            <!-- /.form-group -->
                                        </form>
                                        <!-- /form-->
                                    </div>
                                    <!-- /.content-->
                                </div>
                                <!-- /#form-faq-->
                            </section>
                            <!-- /#faq-form-->
                            <article class="faq-single">
                                <i class="fa fa-question-circle"></i>
                                <div class="wrapper">
                                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel viverra tellus,
                                        at porttitor turpis. Vivamus ultricies orci quam, vitae pharetra lectus scelerisque nec.
                                    </h4>
                                    <div class="answer">
                                        <figure>Answer</figure>
                                        <p>
                                            Vivamus ut turpis risus. Vestibulum gravida dictum sem a rutrum. Fusce tincidunt
                                            rhoncus nulla vel rutrum. Nunc vel luctus dui. Phasellus egestas interdum lacinia.
                                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                            himenaeos. Nam ut commodo nulla, eu vulputate tellus.
                                        </p>
                                    </div>
                                </div>
                            </article>
                            <!-- /.faq-single-->
                            <article class="faq-single">
                                <i class="fa fa-question-circle"></i>
                                <div class="wrapper">
                                    <h4>Morbi vehicula mauris vel bibendum molestie. Ut varius purus in odio elementum
                                    </h4>
                                    <div class="answer">
                                        <figure>Answer</figure>
                                        <p>
                                            Sed placerat porta pretium. Sed in mauris at quam egestas vehicula. Cum sociis
                                            natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                            Aliquam ac eleifend sapien. Quisque tempus dictum urna, in finibus erat luctus sed.
                                        </p>
                                    </div>
                                </div>
                            </article>
                            <!-- /.faq-single-->
                            <article class="faq-single">
                                <i class="fa fa-question-circle"></i>
                                <div class="wrapper">
                                    <h4>Donec condimentum neque est, quis finibus velit laoreet vel.
                                    </h4>
                                    <div class="answer">
                                        <figure>Answer</figure>
                                        <p>
                                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                            Integer molestie viverra neque sit amet posuere. Nullam eget ultricies mi. Proin pulvinar
                                            quam porttitor consequat pulvinar. Etiam non neque et quam euismod cursus. Praesent eu
                                            sem interdum, pharetra metus sed, sollicitudin sem. Curabitur tincidunt dolor
                                            quis dolor iaculis, ut maximus ante fermentum
                                        </p>
                                    </div>
                                </div>
                            </article>
                            <!-- /.faq-single-->
                            <article class="faq-single">
                                <i class="fa fa-question-circle"></i>
                                <div class="wrapper">
                                    <h4>Suspendisse et enim semper, porta nunc ut, ullamcorper magna. Duis ut metus eu
                                        neque sollicitudin cursus non quis leo. Cras quis dignissim magna
                                    </h4>
                                    <div class="answer">
                                        <figure>Answer</figure>
                                        <p>
                                            Vivamus ut turpis risus. Vestibulum gravida dictum sem a rutrum. Fusce tincidunt
                                            rhoncus nulla vel rutrum. Nunc vel luctus dui. Phasellus egestas interdum lacinia.
                                        </p>
                                    </div>
                                </div>
                            </article>
                            <!-- /.faq-single-->
                            <article class="faq-single">
                                <i class="fa fa-question-circle"></i>
                                <div class="wrapper">
                                    <h4>Praesent scelerisque consectetur velit luctus fermentum. Sed lacinia ut nunc id
                                        consectetur. Praesent dignissim, enim in fringilla laoreet, est erat maximus nibh,
                                        id auctor sapien nisl a nisi.
                                    </h4>
                                    <div class="answer">
                                        <figure>Answer</figure>
                                        <p>
                                            Donec at arcu purus. Suspendisse laoreet quam ut purus aliquam, tempor pharetra
                                            tellus maximus. Etiam semper velit tincidunt, accumsan neque non, blandit metus.
                                            Aenean blandit felis at luctus condimentum. Nam gravida luctus mi, sed aliquet
                                            ligula consectetur a. Nulla eget semper felis, eget iaculis ante. Aliquam tempus
                                            molestie quam vel pellentesque. Aliquam nibh turpis, lobortis tempor euismod in,
                                            faucibus ut eros. Cras accumsan gravida tellus.
                                        </p>
                                    </div>
                                </div>
                            </article>
                            <!-- /.faq-single-->
                        </div>
                        <!--Sidebar-->
                        <div class="col-md-3">
                            <aside id="sidebar">
                                <section>
                                    <header><h2>New Places</h2></header>
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Cash Cow Restaurante</h3>
                                        <figure>63 Birch Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="images/items/1.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                                <div class="rating" data-rating="4"></div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Blue Chilli</h3>
                                        <figure>2476 Whispering Pines Circle</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="images/items/2.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                                <div class="rating" data-rating="3"></div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Eddie�s Fast Food</h3>
                                        <figure>4365 Bruce Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="images/items/3.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                                <div class="rating" data-rating="5"></div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                </section>
                                <section>
                                    <a href="#"><img src="images/ad-banner-sidebar.png" alt=""></a>
                                </section>
                                <section>
                                    <header><h2>Categories</h2></header>
                                    <ul class="bullets">
                                        <li><a href="#" >Restaurant</a></li>
                                        <li><a href="#" >Steak House & Grill</a></li>
                                        <li><a href="#" >Fast Food</a></li>
                                        <li><a href="#" >Breakfast</a></li>
                                        <li><a href="#" >Winery</a></li>
                                        <li><a href="#" >Bar & Lounge</a></li>
                                        <li><a href="#" >Pub</a></li>
                                    </ul>
                                </section>
                                <section>
                                    <header><h2>Events</h2></header>
                                    <div class="form-group">
                                        <select class="framed" name="events">
                                            <option value="">Select Your City</option>
                                            <option value="1">London</option>
                                            <option value="2">New York</option>
                                            <option value="3">Barcelona</option>
                                            <option value="4">Moscow</option>
                                            <option value="5">Tokyo</option>
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </section>
                            </aside>
                            <!-- /#sidebar-->
                        </div>
                        <!-- /.col-md-3-->
                        <!--end Sidebar-->
                    </div>
                </section>
            </div>
</div>

<?php include "inc_footer.php" ?>
<script>
	var $ = jQuery.noConflict();
    if( $('body').hasClass('navigation-fixed') ){
        $('.off-canvas-navigation').css( 'top', - $('.header').height() );
        $('#page-canvas').css( 'margin-top',$('.header').height() );
    }
	$(document).ready(function($) {
		$('.off-canvas-navigation header').css( 'line-height', $('.header').height() + 'px' );
		"use strict";
		$(document).bind('keypress', 'M', function(){ 
			$('.header .toggle-navigation').trigger('click');
			return false;
		});
	});
    $( "#redefine-search-form" ).load( "assets/external/_search-bar.html", function() {
        setInputsWidth();
        //autoComplete();
    });	

</script>
<?php include "inc_header.php" ?>
<style>
	.fieldsetmaps{
		background:yellow;
		margin-left:50px;
		display:none;
	}
	.fieldsetmaps input {
		height:10px;
		width:100px;
	}
    .map_canvas{
		display:block;
		width:100%;
		height:300px;
	} 
</style>
 
<div class="bg_content pagestyle pagecontent">
	<div class="container search-bar horizontal collapse in">
		<?php include "inc_search.php" ?>
	</div> 
	<div class="container signupsection">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
				<header>
					<h1 class="page-title">Daftar</h1>
				</header>
				<hr>
				<form role="form" id="form-register" method="post" action="?">
						<div class="form-group">
							<label for="form-register-email">Email:</label>
							<input type="email" class="form-control" id="form-register-email" name="form-register-email" required>
						</div><!-- /.form-group -->
						<div class="form-group">
							<label for="form-register-password">Kata Sandi:</label>
							<input type="password" class="form-control" id="form-register-password" name="form-register-password" required>
						</div><!-- /.form-group -->
						<div class="form-group">
							<label for="form-register-confirm-password">Ulangi Kata Sandi:</label>
							<input type="password" class="form-control" id="form-register-confirm-password" name="form-register-confirm-password" required>
						</div><!-- /.form-group -->
						<div class="checkbox pull-left">
							<label>
								<input type="checkbox" name="newsletter"><span style="margin-left:25px;">Terima newslater.</span>
							</label>
						</div>
						<div class="form-group clearfix">
							<button type="submit" class="btn pull-right btn-default" id="account-submit">Daftar</button>
						</div><!-- /.form-group -->
				</form>
				<hr>
				<div class="center">
					<figure class="note text-center">By clicking the “Create an Account” button you agree with our <a href="terms-conditions.html" class="link">Terms and conditions</a></figure>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<?php include "inc_footer.php" ?>
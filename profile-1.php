                    <div class="row">
                        <div class="col-md-9">
                            <form id="form-profile" role="form" method="post" action="?" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <section>
                                            <h3><i class="fa fa-image"></i>Foto Profile</h3>
                                            <div id="profile-picture" class="profile-picture dropzone">
                                                <input name="file" type="file">
                                                <div class="dz-default dz-message"><span>Klik atau tarik gambar ke sini</span></div>
                                                <img src="css/images/member-2.jpg" alt="">
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-md-9 col-sm-9">
                                        <section>
                                            <h3><i class="fa fa-user"></i>Identitas Diri</h3>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">Nama Lengkap</label>
                                                        <input type="text" class="form-control" id="name" name="name" value="Aldi Ariansyah">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" class="form-control" id="email" name="email" value="aldi@pasaran.com" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="phone">No Handphone</label>
                                                        <input type="text" class="form-control" id="phone" name="phone" pattern="\d*" value="08383892928382">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="pin-bb">Pin BB</label>
                                                        <input type="text" class="form-control" id="pin-bb" name="pin-bb" pattern="\d*" value="021 8282828282">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <input type="checkbox" class="whatsapp pull-left" id="whatsapp" name="whatsapp" checked>
                                                        <label for="whatsapp">Saya bisa dihubungi via whatsapp <img src="css/images/whatsapp.png" alt="whatsapp"></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                         <button type="submit" class="btn btn-large btn-default pull-right" id="submit">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3 col-sm-9">
                            <h3><i class="fa fa-asterisk"></i>Ubah Password</h3>
                            <form class="framed" id="form-password" role="form" method="post" action="?" >
                                <div class="form-group">
                                    <label for="current-password">Password Saat Ini</label>
                                    <input type="password" class="form-control" id="current-password" name="current-password">
                                </div>
                                <div class="form-group">
                                    <label for="new-password">Password Baru</label>
                                    <input type="password" class="form-control" id="new-password" placeholder="Minimal 8 karakter" name="new-password">
                                </div>
                                <div class="form-group">
                                    <label for="confirm-new-password">Ulangi Password Baru</label>
                                    <input type="password" class="form-control" id="confirm-new-password" placeholder="Minimal 8 karakter" name="confirm-new-password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default">Ubah Password</button>
                                </div>
                            </form>
                        </div>
                    </div>
	<div class="clear"></div> 
	<section id="footer" class="footer_section">
		<footer id="page-footer">
            <div class="inner">
                <div class="footer-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-sm-5 ">
                                <!--New Items-->
                                <section>
									<img src="images/material/logo/pasaran.png" style="width: 200px;margin-top: 10px;" alt="Pasaran Logo"> 
                                    <div class="wrapper">
                                      pasaran.com adalah situs pemasaran kebutuhan apapun dimanapun anda berada. Situs pemasaran No.1 di Indonesia ini memberikan banyak kemudahan terutama untuk pencarian barang, kendaraan, properti, pekerjaan, jasa, dan event disekitar anda.
                                    </div>
                                    <a href="about-us.html" class="read-more icon-readmore">Baca Selengkapnya</a>
                                </section>
                            </div>
                                <!--/.Social-media-->
                            <div class="col-md-2 col-sm-2">
                                <section>
                                        <h2><strong>Follow us</strong></h2>
                                        <a href="#" class="social-button"><i class="fa fa-twitter twitter"></i>Twitter</a>
                                        <a href="#" class="social-button"><i class="fa fa-facebook fb"></i>Facebook</a>
                                        <a href="#" class="social-button"><i class="fa fa-google gplus"></i>Google</a>
                                </section>
                            </div>
                                <!--/.End-social-media-->
                            <div class="col-md-2 col-sm-2">
                                <!--Recent Reviews-->
                                <section>
                                    <h2><strong>Navigasi</strong></h2>
                                    <a href="faq.php" class="small"><h3>FAQ</h3></a>
                                    <a href="blog.php" class="small"><h3>Blog</h3></a>
                                    <a href="about-us.php" class="small"><h3>Tentang Kami</h3></a>
                                    <a href="terms-conditions.php" class="small"><h3>Syarat dan Ketentuan</h3></a>
                                </section>
                                <!--end Recent Reviews-->
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <section>
                                    <h2><strong>Tentang Kami</strong></h2>
                                    <address>
                                        <div>Kencana Tower Lt 7</div>
                                        <div>Business Park Kebon Jeruk</div>
                                        <div>Jl. Meruya Ilir Raya No. 88, Kembangan</div>
                                        <div>Jakarta Barat, 11620</div>
                                        <figure>
                                            <div class="info">
                                                <i class="fa fa-phone"></i>
                                                <span>(021)30061568</span>
                                            </div>
                                        </figure>
                                    </address>
                                    <a href="contact.html" class="btn framed icon">Hubungi Kami<i class="fa fa-angle-right"></i></a>
                                </section>
                            </div>
                            <!--/.col-md-4-->
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </div>
                <!--/.footer-top-->
                <div class="footer-bottom">
                    <div class="container">
                        <span class="left">Copyright © 2016 Pasaran.com. All Rights Reserved</span>
                            <span class="right">
                                <a href="#page-top" class="to-top roll"><i class="fa fa-angle-up"></i></a>
                            </span>
                    </div>
                </div>
                <!--/.footer-bottom-->
            </div>
        </footer>
	</section>	
	<div class="clear"></div>
  
	<a class="page-scroll" href="#page-top">
		<div id="scrolltotop" class="scrolltotop">
			<i class="fa fa-chevron-up"></i>
		</div>
	</a>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=places"></script>	
	<script type="text/javascript" src="js/richmarker-compiled.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<script src="js/owl.carousel.js"></script>
    <script src="js/js_lib.js"></script>
    <script src="js/js_run.js"></script>
    <script src="js/TweenMax.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.min.js"></script> 
	<script src="js/dropzone.js"></script>
	<script src="js/jquery.geocomplete.js"></script> 
	<script src="js/before.load.js"></script> 
	<script>
		var $ = jQuery.noConflict();
		$(document).on('click', '.to-top', function () { 
			$('html, body').animate({ scrollTop: 0 }, 500);
		});
		$('.expand-content').live('click',  function(e){
			e.preventDefault();
			var children = $(this).attr('data-expand');
			var parentHeight = $(this).closest('.expandable-content').height();
			var contentSize = $( children + ' .content' ).height();
			$( children ).toggleClass('collapsed');
			$( this ).toggleClass('active');
			$( children ).css( 'height' , contentSize );
			if( !$( children).hasClass('collapsed') ){
				setTimeout(function() {
					$( children ).css('overflow', 'visible');
				}, 400);
			}
			else {
				$( children ).css('overflow', 'hidden');
			}
			$('.has-child').live('click',  function(e){
				var parent = $(this).closest('.expandable-content');
				var childHeight = $( $(this).attr('data-expand') + ' .content').height();
				if( $(this).hasClass('active') ){
					$(parent).height( parent.height() + childHeight )
				}
				else {
					$(parent).height(parentHeight);
				}
			});
		});
		$(function(){
			if( $('.dropzone').length > 0 ) {
				Dropzone.autoDiscover = false;
				$("#file-submit").dropzone({
					url: "upload",
					addRemoveLinks: true,
					acceptedFiles: "image/jpeg,image/jpg,image/png"
				});

				$("#photo-edit").dropzone({
					url: "upload",
					addRemoveLinks: true,
					acceptedFiles: "image/jpeg,image/jpg,image/png",
					thumbnailWidth: 200,
					thumbnailHeight: 200
				});

				$("#profile-picture").dropzone({
					url: "upload",
					addRemoveLinks: true,
					acceptedFiles: "image/jpeg,image/jpg,image/png"
				});
			}		
			$("#geocomplete").geocomplete({
				map: ".map_canvas",
				details: "form",
				types: ["geocode", "establishment"],
			});

			$("#find").click(function(){
				$("#geocomplete").trigger("geocode");
			});
		}); 
		/*$.getJSON('file', function(data) {
		  $.each(data, function(index, val) {
			var mockFile = { name: val.name, size: val.size };
			thisDropzone.options.addedfile.call(thisDropzone, mockFile);
			thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "file/" + val.name);
		  });
		});*/

		function initializeOwl(_rtl){
			$.getScript( "js/owl.carousel.min.js", function( data, textStatus, jqxhr ) {
				if ($('.owl-carousel').length > 0) {
					if ($('.carousel-full-width').length > 0) {
						setCarouselWidth();
					}
					$(".carousel.wide").owlCarousel({
						rtl: _rtl,
						items: 1,
						responsiveBaseWidth: ".slide",
						nav: true,
						navText: ["",""]
					});
					$(".item-slider").owlCarousel({
						rtl: _rtl,
						items: 1,
						autoHeight: true,
						responsiveBaseWidth: ".slide",
						nav: false,
						callbacks: true,
						URLhashListener: true,
						navText: ["",""]
					});
					$(".list-slider").owlCarousel({
						rtl: _rtl,
						items: 1,
						responsiveBaseWidth: ".slide",
						nav: true,
						navText: ["",""]
					});
					$(".testimonials").owlCarousel({
						rtl: _rtl,
						items: 1,
						responsiveBaseWidth: "blockquote",
						nav: true,
						navText: ["",""]
					});

					$('.item-gallery .thumbnails a').on('click', function(){
						$('.item-gallery .thumbnails a').each(function(){
							$(this).removeClass('active');
						});
						$(this).addClass('active');
					});
					$('.item-slider').on('translated.owl.carousel', function(event) {
						var thumbnailNumber = $('.item-slider .owl-item.active img').attr('data-hash');
						$( '.item-gallery .thumbnails #thumbnail-' + thumbnailNumber ).trigger('click');
					});
					return false;
				}
			});
		}
    $(window).load(function(){
        var rtl = false; // Use RTL
        initializeOwl(rtl);
    });		
	</script>
</body> 
</html>

<!-- Navigation -->
<div class="clear"></div> 
<div class="navigation_bar">
	<div class="container"> 
			<nav class="navbar navbar-default " role="navigation"> 
					<div class="navbar-header page-scroll">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!--<a class="navbar-brand page-scroll img_logo" href="#page-top"><img src="images/material/logo.png" class="img-responsive"/></a>-->
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<div class="row">
							<ul class="nav navbar-nav"> 
								<li>
									<a href="#">
										<i class="fa fa-shopping-cart"></i> Belanja
									</a> 
								</li>
								<li>
									<a href="#">
										<i class="fa fa-building"></i> Properti
									</a> 
								</li>
								<li>
									<a href="#">
										<i class="fa fa-car"></i> Otomotif
									</a> 
								</li>
								<li>
									<a href="#">
										<i class="fa fa-wrench"></i> Jasa
									</a> 
								</li>
								<li>
									<a href="#">
										<i class="fa fa-briefcase"></i> Pekerjaan
									</a> 
								</li>
								<li>
									<a href="#">
										<i class="fa fa-users"></i> Event
									</a> 
								</li>
								 
							</ul>
							<ul class="nav navbar-nav navbar-right">  
								<li><a class="btn" href="#"><i class="fa fa-plus"></i>Pasang Iklan</a></li>  
							</ul> 
						</div>  
					</div>  
			</nav> 
	</div>
</div>
<div class="clear"></div> 
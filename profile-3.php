                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <aside id="sidebar">
                                <ul class="navigation-sidebar list-unstyled">
                                    <li class="active">
                                        <a href="#">
                                            <i class="fa fa-envelope"></i>
                                            <span>Semua Pesan</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-inbox"></i>
                                            <span>Kotak masuk</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-paper-plane"></i>
                                            <span>Kotak Keluar</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa fa-archive"></i>
                                            <span>Arsip</span>
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <section id="items">
                                <div class="item list admin-view">
                                    <div class="wrapper">
                                        <a href="mailbox-details.html"><h3>Gan itu yang motor yamaha masih bisa di nego ga ya gan ?</h3></a>
                                        <figure>Pesan dari <a href="#">Ariansyah</a> di Jakarta</figure></br>
                                        <div class="content">
                                            <p>Gan itu yang motor yamaha masih bisa di nego ga ya gan ?</p>
                                            <p>klo boleh ane bawa gan :D</p>
                                        </div></br>
                                    </div>
                                    <div class="ribbon in-queue">
                                        <i class="fa fa-archive" data-toggle="tooltip" title="Pesan Sudah Diarsipkan"></i>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
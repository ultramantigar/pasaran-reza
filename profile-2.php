					<div class="row">
                        <div class="col-md-3 col-sm-3">
                            <aside id="sidebar">
                                <ul class="navigation-sidebar list-unstyled">
                                    <li class="active">
                                        <a href="iklan-saya.html">
                                            <i class="fa fa-folder"></i>
                                            <span>Semua Iklan Saya</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="iklan-aktif.html">
                                            <i class="fa fa-check"></i>
                                            <span>Aktif</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="iklan-queue.html">
                                            <i class="fa fa-clock-o"></i>
                                            <span>Menunggu verifikasi</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="iklan-non-aktif.html">
                                            <i class="fa fa-eye-slash"></i>
                                            <span>Tidak Aktif</span>
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <section id="items">
                                <div class="item list admin-view">
                                    <div class="image">
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span title="timestamp">Hari Ini (Timestamp)</span>
                                            </div>
                                            <img src="css/images/1.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Cash Cow Restaurante</h3></a>
                                        <figure>63 Birch Street</figure>
                                        <div class="info">
                                            <div class="type">
                                                <i class="fa fa-eye"></i>
                                                <span>Dilihat 1000 kali</span>
                                            </div>
                                            <div class="type">
                                                <a href="#"><i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                <span>{nama Kategorinya}</span></a>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="description">
                                        <ul class="list-unstyled actions">
                                            <li><a href="edit-iklan.html" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                            <li><a href="#" data-toggle="tooltip" title="Non-Aktifkan"><i class="fa fa-eye-slash"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="ribbon approved">
                                        <i class="fa fa-check"></i>
                                    </div>
                                </div>
                                <!-- /.item-->
                                <div class="item list admin-view">
                                    <div class="image">
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span title="timestamp">Hari Ini (Timestamp)</span>
                                            </div>
                                            <img src="css/images/1.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Cash Cow Restaurante</h3></a>
                                        <figure>63 Birch Street</figure>
                                        <div class="info">
                                            <div class="type">
                                                <i class="fa fa-eye"></i>
                                                <span>Dilihat 1000 kali</span>
                                            </div>
                                            <div class="type">
                                                <a href="#"><i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                <span>{nama Kategorinya}</span></a>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="description">
                                        <ul class="list-unstyled actions">
                                            <li><a href="edit-iklan.html" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                            <li><a href="#" data-toggle="tooltip" title="Non-Aktifkan"><i class="fa fa-eye-slash"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="ribbon in-queue">
                                         <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.item-->
                                <div class="item list admin-view">
                                    <div class="image">
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span title="timestamp">Hari Ini (Timestamp)</span>
                                            </div>
                                            <img src="css/images/1.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Cash Cow Restaurante</h3></a>
                                        <figure>63 Birch Street</figure>
                                        <div class="info">
                                            <div class="type">
                                                <i class="fa fa-eye"></i>
                                                <span>Dilihat 1000 kali</span>
                                            </div>
                                            <div class="type">
                                                <a href="#"><i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                <span>{nama Kategorinya}</span></a>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="description">
                                        <ul class="list-unstyled actions">
                                            <li><a href="#" data-toggle="tooltip" title="Aktifkan"><i class="fa fa-check"></i></a></li>
                                            <li><a href="edit-iklan.html" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a></li>
                                            <li><a href="#" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="ribbon in-non-aktif">
                                        <i class="fa fa-eye-slash"></i>
                                    </div>
                                </div>
                                <!-- /.item-->
                            </section>
                        </div>
                    </div>